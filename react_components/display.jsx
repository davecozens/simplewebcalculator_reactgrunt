/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({displayName: "Display",
  render: function() {
    return (
      React.createElement("div", {className: "display"}, React.createElement("div", null, 
        this.props.buffer
      ))
    );
  }
});
