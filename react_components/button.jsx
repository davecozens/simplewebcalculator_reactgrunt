/** @jsx React.DOM */

var React = require('react');

module.exports = React.createClass({displayName: "Button",
  onClick: function(){
    this.props.onClick(this.props.label);
  },
  onTouchStart: function(){
    this.props.onClick(this.props.label);
  },
  render: function() {
    return (
      React.createElement("div", {className: "button", onClick: this.onClick, className: this.props.type}, React.createElement("div", null, 
        this.props.label
      ))
    );
  }
});
